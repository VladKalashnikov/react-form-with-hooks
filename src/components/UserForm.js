import React, { useState, useReducer } from 'react'
import FormUserDetails from './FormUserDetails'
import FormPersonalDetails from './FormPersonalDetails'
import Confirm from './Confirm'
import Success from './Success'

const UserForm = (props) => {

    const [step, dispatchStep] = useReducer((prevStep, action) => {
        switch (action) {
            case 'decrement':
                return prevStep -= 1;
            case 'increment':
                return prevStep += 1;
            default:
                throw new Error('Unexpected action');
        }
    }, 1)

    const [formData, setformData] = useState({
        firstName: '',
        lastName: '',
        email: '',
        occupation: '',
        city: '',
        bio: ''
    });

    // Handle fields change
    const handleChange = input => e => {
        e.persist();
        setformData(prevFormData => {
            return { ...prevFormData, ...{ [input]: e.target.value } }
        });
    }

    // Go forward to next step
    const nextStep = () => {
        dispatchStep('increment');
    }

    // Go back to previous step
    const prevStep = () => {
        dispatchStep('decrement');
    }

    switch (step) {
        case 1:
            return (
                <FormUserDetails
                    nextStep={nextStep}
                    handleChange={handleChange}
                    formData={formData}
                />
            );
        case 2:
            return (
                <FormPersonalDetails
                    prevStep={prevStep}
                    nextStep={nextStep}
                    handleChange={handleChange}
                    formData={formData}
                />
            );
        case 3:
            return (
                <Confirm
                    prevStep={prevStep}
                    nextStep={nextStep}
                    formData={formData}
                />
            );
        case 4:
            return <Success />;
        default:
            throw new Error("Unexpected 'step' value")
    }
}

export default UserForm
