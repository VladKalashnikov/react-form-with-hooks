import React from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from 'material-ui/AppBar'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

const FormPersonalDetails = props => {

    const backHandler = e => {
        e.preventDefault();
        props.prevStep();
    }

    const continueHandler = e => {
        e.preventDefault();
        props.nextStep();
    }

    const { formData, handleChange } = props;

    return (
        <MuiThemeProvider>
            <React.Fragment>
                <AppBar title="Enter Personal Details" />
                <TextField
                    hintText="Enter Your Occupation"
                    floatingLabelText="Occupation"
                    onChange={handleChange('occupation')}
                    defaultValue={formData.occupation}
                />
                <br />
                <TextField
                    hintText="Enter Your City"
                    floatingLabelText="City"
                    onChange={handleChange('city')}
                    defaultValue={formData.city}
                />
                <br />
                <TextField
                    hintText="Enter Your Bio"
                    floatingLabelText="Bio"
                    onChange={handleChange('bio')}
                    defaultValue={formData.bio}
                />
                <br />
                <RaisedButton
                    label="Back"
                    primary={false}
                    style={styles.button}
                    onClick={backHandler}
                />
                <RaisedButton
                    label="Continue"
                    primary={true}
                    style={styles.button}
                    onClick={continueHandler}
                />
            </React.Fragment>
        </MuiThemeProvider>
    )

}

const styles = {
    button: {
        margin: 15
    }
}

export default FormPersonalDetails
