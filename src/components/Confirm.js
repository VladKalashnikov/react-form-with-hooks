import React from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from 'material-ui/AppBar'
import { List, ListItem } from 'material-ui/List'
import RaisedButton from 'material-ui/RaisedButton'

const Confirm = props => {

    const backHandler = e => {
        e.preventDefault();
        props.prevStep();
    }

    const continueHandler = e => {
        e.preventDefault();
        props.nextStep();
    }

    const { formData: { firstName, lastName, email, occupation, city,
        bio } } = props;

    return (
        <MuiThemeProvider>
            <React.Fragment>
                <AppBar title="Confirm User Data" />
                <List>
                    <ListItem
                        primaryText="First Name"
                        secondaryText={firstName}
                    />
                    <ListItem
                        primaryText="Last Name"
                        secondaryText={lastName}
                    />
                    <ListItem
                        primaryText="Email"
                        secondaryText={email}
                    />
                    <ListItem
                        primaryText="Occupation"
                        secondaryText={occupation}
                    />
                    <ListItem
                        primaryText="City"
                        secondaryText={city}
                    />
                    <ListItem
                        primaryText="Bio"
                        secondaryText={bio}
                    />
                </List>
                <br />
                <RaisedButton
                    label="Back"
                    primary={false}
                    style={styles.button}
                    onClick={backHandler}
                />
                <RaisedButton
                    label="Confirm and continue"
                    primary={true}
                    style={styles.button}
                    onClick={continueHandler}
                />
            </React.Fragment>
        </MuiThemeProvider>
    )

}

const styles = {
    button: {
        margin: 15
    }
}

export default Confirm
